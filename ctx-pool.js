var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var timer = setInterval(draw, 100);

var pi = Math.PI;
canvas.style.backgroundColor = "#f0fff0";

var xmax = canvas.width;
var ymax = canvas.height;
var t = {
	xmin: 4, xmax: xmax -4,
	ymin: 4, ymax: ymax - 34,
};
t.xmid = (t.xmax - t.xmin) / 2 + t.xmin;
t.ymid = (t.ymax - t.ymin) / 2 + t.ymin;
t.xcue = (t.xmax - t.xmin) / 4 * 3 + t.xmin;
t.xrack = (t.xmax - t.xmin) / 4 + t.xmin;

var ballsz = 12;

var drag = 0.99;
var velmin = 0.01;



var balls = [
	new CBall(400,150,-40,1,  "White", false,''),

	new CBall(130,150,0,0,  "Yellow", false,1),
	new CBall(90,170,0,0, 	"Blue", false,2),
	new CBall(70,140,0,0, 	"Crimson", false,3),
	new CBall(70,180,0,0,  	"Purple", false,4),
	new CBall(50,170,0,0, 	"DarkOrange", false,5),
	new CBall(110,140,0,0,  "LimeGreen", false,6),
	new CBall(50,130,0,0, 	"Brown", false,7),
	new CBall(90,150,0,0,  	"Black", false,8),
	new CBall(110,160,0,0,  "Yellow", true,9),
	new CBall(70,120,0,0, 	"Blue", true,10),
	new CBall(50,110,0,0, 	"Crimson", true,11),
	new CBall(70,160,0,0, 	"Purple", true,12),
	new CBall(50,150,0,0, 	"DarkOrange", true,13),
	new CBall(90,130,0,0,  	"LimeGreen", true,14),
	new CBall(50,190,0,0, 	"Brown", true,15),
];

var pockets = [
	new CPocket(t.xmin, t.ymin, 0.7,  1, 1, 0,1),
	new CPocket(t.xmid, t.ymin, 0.5,  0, 1, 0,2),
	new CPocket(t.xmax, t.ymin, 0.7, -1, 1, 1,2),
	new CPocket(t.xmin, t.ymax, 0.7,  1,-1, 3,4),
	new CPocket(t.xmid, t.ymax, 0.5,  0,-1, 2,4),
	new CPocket(t.xmax, t.ymax, 0.7, -1,-1, 2,3),
];

var ballsoff = [];

rack();


var clickready = true;
var ballsel = -1;
canvas.onmouseup = function(e) {
	clickready = true;

	if(ballsel>=0){
		if(balls[ballsel].ontable){
			balls[ballsel].vx = rand(10)
			balls[ballsel].vy = rand(10)
		}
		else{
			var x = e.pageX - canvas.offsetLeft;
			var y = e.pageY - canvas.offsetTop;
			balls[ballsel].setat(x,y,0,0,0);
		}
		ballsel = -1;
	}
};
canvas.onmousedown = function(e) {
	var g = false;
	if(clickready){
		clickready = false;
		var x = e.pageX - canvas.offsetLeft;
		var y = e.pageY - canvas.offsetTop;
		var sz = ballsz

		// ball grabbing
		for(i=0; i<balls.length; i++){
			var x0 = x - balls[i].x
			var y0 = y - balls[i].y

			if(x0+sz >= 0 && x0-sz <= 0
			&& y0+sz >= 0 && y0-sz <= 0){
				if(balls[i].ontable){
					balls[i].x = x
					balls[i].y = y
					balls[i].vx = 0;
					balls[i].vy = 0; 
					balls[i].spin = 0;
				}
				ballsel = i
				g = true;
				break;
			}
		}

		//rack button
		var x0 = bt_r[0];
		var y0 = bt_r[1];
		var x1 = bt_r[2];
		var y1 = bt_r[3];
		if(x0<x && x1>x && y0<y && y1>y){ rack();  g = true; }

		//make shot
		if(! g){
			balls[0].vx = (balls[0].x - x)/2;
			balls[0].vy = (balls[0].y - y)/2;
			
		}
	}
};




function CPocket(x,y, ofss,ofsx,ofsy, r0,r1){
	this.sz = 1.2 * ballsz;
	this.ex = x; //corner/edge
	this.ey = y;
	var ofs = this.sz * ofss;
	this.cx = x + ofs * ofsx; //pocket center
	this.cy = y + ofs * ofsy;
	this.r0 = pi/2 * r0 - ofss;
	this.r1 = pi/2 * r1 + ofss;

	this.drw = function(){
		ctx.fillStyle = "DarkGreen";
		ctx.beginPath();
		ctx.moveTo(this.ex, this.ey);
		ctx.arc(this.cx, this.cy, this.sz, this.r0, this.r1, false);
		ctx.fill();
	}

	this.isin = function(ball){
		var dx = this.cx - ball.x;
		var dy = this.cy - ball.y;
		var sz = this.sz;
		var d = Math.sqrt(dx*dx + dy*dy);
		var i = (d - sz) < 0;
		if(i){ ball.setoff(); }
		return i;
	}

	return this;
};



function draw(){
	ctx.clearRect(0,0, xmax,ymax);
	ctx.strokeRect(0,0, xmax,ymax);

	//table
	ctx.strokeRect(t.xmin,t.ymin, t.xmax-t.xmin,t.ymax-t.ymin);
	
	//rack button
	triangle(bt_r[4],bt_r[1], bt_r[0],bt_r[3], bt_r[2],bt_r[3])

	//pockets
	for(i=0; i<pockets.length; i++){
		pockets[i].drw();
	}

	//balls
	for(i=0; i<balls.length; i++){
		balls[i].drw();
		balls[i].update();
		for(j=0; j<pockets.length; j++){
			pockets[j].isin(balls[i]);
		}
	}
	//ball collision test -- each ball pair only once
	for(i=0; i<balls.length-1; i++){
	for(j=i; j<balls.length; j++){
		if(i!=j){ balls[i].collide_test(balls[j]); }
	}}

};

var bt_r = [20,t.ymax+7, 40,ymax-5, 30];

function rack(){
	var x0 = t.xcue;
	var x = t.xrack;
	var y = t.ymid;
	var sy = ballsz + 0.00001;
	var sx = ballsz * 1.9 + 0.00001;

	var xx1 = x;
	var xx2 = x - sx;
	var xx3 = x - sx*2;
	var xx4 = x - sx*3;
	var xx5 = x - sx*4;

	var o1 = sy;
	var o2 = sy*2;
	var o3 = sy*3;
	var o4 = sy*4;

	var p = [[x0,y], 
		[xx1,y], 
		[xx2,y-o1], [xx2,y+o1],
		[xx3,y-o2], [xx3,y],    [xx3,y+o2],
		[xx4,y-o3], [xx4,y-o1], [xx4,y+o1], [xx4,y+o3],
		[xx5,y-o4], [xx5,y-o2], [xx5,y],    [xx5,y+o2], [xx5,y+o4] ];

	var order = [0, -1, -1,-2, -2,8,-1, -1,-2,-1,-2, -2,-1,-2,-2,-1 ];
	var solid = shuf([1,2,3,4,5,6,7]);
	var stripe = shuf([9,10,11,12,13,14,15]);
	
	for(i=0; i<order.length; i++){
		var o = order[i];

		if(o == -1){o = solid.pop();}
		if(o == -2){o = stripe.pop();}

		balls[o].setat(p[i][0],p[i][1],0,0,0);
	}
	ballsoff = [];
}


function CBall(x,y,vx,vy,col,s,n){
	this.x = x;
	this.y = y;
	this.vx = vx;
	this.vy = vy;
	this.sz = ballsz;
	this.col = col;
	this.s  = s;
	this.angle = 0;
	this.spin = 0;
	this.n = n;
	this.ontable = true;

	this.drw = function(){
		ctx.fillStyle = "White";
		ctx.beginPath();
		ctx.arc(this.x,this.y, this.sz, 0, 2*pi, true);
		ctx.fill();

		ctx.fillStyle   = this.col;

		ctx.beginPath();

		var s = 0; if(this.s){ s = 0.6; }
		var sp =  this.angle;
		ctx.arc(this.x,this.y, this.sz, sp+s, pi+sp-s, true);
		ctx.arc(this.x,this.y, this.sz, sp-s, pi+sp+s, false);

		ctx.fill();

		ctx.fillStyle = "Black";
		ctx.strokeStyle = "Black";
		ctx.beginPath();
		ctx.arc(this.x,this.y, this.sz, 0, 2*pi, true);
		ctx.stroke();
		ctx.fillText('     '+n,this.x,this.y)

//		var vx = this.vx.toPrecision(1);
//		var vy = this.vy.toPrecision(1);
//		var spin = this.spin.toPrecision(1);
//		ctx.fillText('     vx '+vx,this.x,this.y+10)
//		ctx.fillText('     vy '+vy,this.x,this.y+20)
//		ctx.fillText('     spin '+spin,this.x,this.y+30)
	};

	this.setat = function(x,y,vx,vy,spin){
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
		this.spin = spin;
		this.ontable = true;
	}
	this.setoff = function(){
		this.ontable = false;
		var sz = this.sz;
		var oi = ballsoff.length;
		ballsoff.push(1);

		this.x = (8 + oi*2) * sz;
		this.y = ymax - sz - 4;	
		this.vx = 0;
		this.vy = 0;
		this.spin = 0;
	}

	this.update = function(){
		if(this.ontable){
			var x = this.x + this.vx;
			var y = this.y + this.vy;
			var sz = this.sz;
			var vx = this.vx;
			var vy = this.vy;
			var spin = this.spin;

			var e = vx*vx + vy*vy + Math.abs(spin/10);

			//hit edge
			if(x<t.xmin+sz || x>t.xmax-sz || y<t.ymin+sz || y>t.ymax-sz){
				if(x<t.xmin+sz){      vx *= -1; x = t.xmin+sz; }
				else if(x>t.xmax-sz){ vx *= -1; x = t.xmax-sz; }
				if(y<t.ymin+sz){      vy *= -1; y = t.ymin+sz; }
				else if(y>t.ymax-sz){ vy *= -1; y = t.ymax-sz; }

				//vx += rand(1);
				//vy += rand(1);
				spin = rand(e); //10);
			}

			//drag
			vx *= drag;
			vy *= drag;
			spin *= drag;
		
			if(e < velmin){
				spin = vx = vy = 0; 
			}

			this.vx = vx;
			this.vy = vy;
			this.spin = spin;

			this.x = x;
			this.y = y;
			this.angle += spin;
		}

	};

	this.distance = function(ball){
		var dx = this.x - ball.x;
		var dy = this.y - ball.y;
		var sz = this.sz + ball.sz;
		var d = Math.sqrt(dx*dx + dy*dy);
		return d - sz;
	}

	this.collide_test = function(ball){
		if(this.ontable && ball.ontable){	
			var d = this.distance(ball);
			if(d<0){

				var vx1 = this.vx;
				var vy1 = this.vy;
				var sp1 = this.spin;
				var vx2 = ball.vx;
				var vy2 = ball.vy;
				var sp2 = ball.spin;

				var p1 = 0.8;  //energy transfer
				var p2 = 0.2;
			
				this.vx = vx2*p1 + vx1*p2 + rand(1);
				this.vy = vy2*p1 + vy1*p2 + rand(1);
				var e = vx1*vx2 + vy1*vy2 + Math.abs(sp1/10);
				this.spin = rand(e);

				ball.vx = vx1*p1 + vx2*p2 + rand(1);
				ball.vy = vy1*p1 + vy2*p2 + rand(1);
				var e = vx1*vx2 + vy1*vy2 + Math.abs(sp2/10);
				ball.spin = rand(e);

				//avoid sticking
				this.x += this.vx;
				this.y += this.vy;
				ball.x += ball.vx;
				ball.y += ball.vy;
			}

			var d = this.distance(ball);
			if(d<0){ //still sticking		
				var sz = this.sz + ball.sz;
				var mv =  d * -0.5;

				var dx = this.x - ball.x;
				if(dx < 0){ this.x -= mv; ball.x += mv; }
				else{       this.x += mv; ball.x += mv; }

				var dy = this.y - ball.y;
				if(dy < 0){ this.y -= mv; ball.y += mv; }
				else{       this.y += mv; ball.y += mv; }

			}
		}
	};

	return this;
}



function rand(i){
	var r = (Math.random() - 0.5) * i;
	return r;
}


function triangle(x1,y1,x2,y2,x3,y3){
	ctx.beginPath();
	ctx.moveTo(x1,y1);
	ctx.lineTo(x2,y2);
	ctx.lineTo(x3,y3);
	ctx.closePath();
	ctx.stroke();
}

function shuf(a){
	for(i=0; i<a.length; i++){
		var j = Math.trunc(Math.random() * a.length);
		var t = a[i];
		a[i] = a[j];
		a[j] = t;		
	}
	return a;
}
